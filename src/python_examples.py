# from sample_class import Animal

# Examples

# 1 - Comments
# 2 - Print Statements
# 3 - Ints, Booleans and Strings
# 4 - Type Conversions
# 5 - Lists
# 6 - Converting strings to lists
# 7 - Dictionaries
# 8 - Tuples
# 9 - Sets
# 10 - Slicing
# BREAK 1 - Lists, Slicing, Type Conversions and Expressions
# 11 - If/Elif/Else statements
# 12 - While/For
# 13 - Functions
# 14 - Classes, constructors, instance and static methods
# 15 - Importing and Exporting modules
# 16 - Reading and Writing Files
# BREAK 2 - Writing functions


# This is a comment. Comments can be commented/uncommented with (Command + /)

# 1 - Comments

# 2 - Print statements
# print("Hello")
# print("My name is Jonathan")

# 3 - Ints Booleans and Strings
# Python has type inference, which means you do not need to declare the type of the variable when you initialize it.
# a = "Hello"
# b = r"\tHello"
# c = 5728
# d = True
# e = False
# f = "Alex" if 0 > 1 else "Bob" if -1 > 1 else "Cathy" if -2 > 1 else "Dillon" if 2 > 1 else "Emily"
#
# print(f)

# # 4 - Type Conversions
# a = int("01")
# print(a)
#
# b = str(15)
# print("String for 15: " + b)


# # 5  - Lists
# list_0 = []
# list_1 = [1,2,3,4,5]
#
# # If you only want to iterate over a list:
# for element in list_1:
# 	print(element)
#
# # To get to the index for something in a list (like in Java), use "enumerate"
#
# for key, value in enumerate(list_1):
# 	print("Key: " + str(key) + ", Value: " + str(value))

# 6 - Converting strings to lists

# hello_string = list("Hello")
# for letter in hello_string:
# 	print (letter)

# There are no characters in Python. You must work with strings.

# # 7 - Dictionaries
# my_dict_empty = {}
#
# # Dictionaries can have different types as keys and values.
# my_dict_full = {"Alice": 25, "Bobby": 30, 32: "Hello"}
#
# # To obtain each key and value, use "enumerate"
# for key, value in enumerate(my_dict_full):
# 	print("Key: " + str(key) + ", Value: " + str(value))

# 8 - Tuples

# Tuples are immutable lists. They can be indexed but they cannot be modified. Use () to create a tuple

# tuple_1 = (1,2,3,4)
# tuple_2 = (5,6)
# tuple_3 = tuple([1,2,3,4,5,6,7,8,9]) # Type conversion
#
# print (tuple_1)
# print (tuple_2[1])
# print(tuple_3)

# 9 - Sets
#
# set_1 = set([1,2,3,4,5])
# set_2 = set([3,3,3,3,3,3])
# set_3 = {1,1,1,1,2,3,4}
#
# print (set_1)
# print (set_2)
# print (set_3)
#
# # Since sets require unique elements, for loops will only print out unique elements of sets.
# for element in set_2:
# 	print(element)
#
# # 10 - Slicing
# # Slicing is an advanced technique that allows you to pick parts of a list, set or tuple.
#
# # Slicing lists give you parts of a list:
# list_a = [0,1,2,3,4,5,6,7]
# print(list_a[1:4])
# print(list_a[0:8])
#
# # Can use negative indices as well:
# print(list_a[1:-1])
# print(list_a[:-2])
# print(list_a[-2:])
# print(list_a[:])
#
# # Can reverse lists as well:
# print(list_a[::-1])
# print(list_a[::-2])
# print(list_a[-2:1:-2])
#
# # Advanced technique - Slicing isn't required.
#
# # Can be done with tuples as well:
# tuple_10 = (0,1,2,3,4,5,6)
# print(tuple_10[::-1])

# BREAK 1: Do the following:

# A - Print the answer of (1+3)-2/34:

# B - Reverse and print the string for "Hello, this is not a palindrome":

# # C - Given the following list, take the first half of the list, reverse it, and print every 2 elements from the starting point of the reversed list, all in one line:
# my_example_list = [10,20,30,40,50,60,70,80]

# # 11 - If/Elif/Else Statements
#
# # if statements do not require parentheses, but do require a colon:
#
# if 5 % 1 == 0:
# 	print ("Statement works")
# elif 5 % 1 == 1:
# 	print ("Made a boo boo")
# else:
# 	print ("What's going on...")
#
# # Can use and, or, not, is, ==, <=, >=, <, > just like in Java.
#
# if 1 > 0 and 0 > 1:
# 	print ("Contradiction!")
# if 1 > 0 and not 0 > 1:
# 	print ("Statement works")
# if int("01") is 1:
# 	print ("Casting and is works.")

# 12 - While/For:

# while loops are just like if statements but with iteration
# x = 0
# while x < 10:
# 	print(x)
# 	x += 1
#
# # to do a classic for loop in Java, range creates a list that is INCLUSIVE from the LEFT and EXCLUSIVE of the RIGHT:
# for x in range(10, 20):
# 	print (x)


# prints 10 to 19

# 13 - Functions:

# Functions can be GLOBAL in Python - they do not need to be in a class.

# def say_hello(name):
# 	return "Hello, " + name
# print(say_hello("Timmy"))

# To get type safety in Python as in Java, provide "type hints":
#
# def say_hello_strict(name: str) -> str:
# 	return name
# print(say_hello_strict("James"))
# print(say_hello_strict(19)) 		# Raises warning.

# Type hints are good, because:
	# They allow for the IDE to recognize the return type for a function, so a returned object's type is known at COMPILE TIME. Makes it easier to detect errors.
	# Makes code-completion easier which is big for large projects.

# 14 - Classes:
# dog = Animal(name="Spot", weight=90)
# print(dog.get_weight_in_pounds())
# print(Animal.get_type(dog.name))
#
# squid = Animal(name="Tommy", weight=1)
# print(squid.get_weight_in_pounds())
# print(Animal.get_type(squid.name))

# 15 - To use a class from another module (modules are just files), use the following syntax at the beginning:

# from file_name import class_name

# You might need to make sure your folder "/src" is marked as the sources root, or else the IDE might not be able to find it.

# 16 - Reading and writing files.

# Much easier than Java.

# my_input_file = open('sample.text', 'r')
#
# # print(my_input_file.read())
#
# # print(my_input_file.readlines())
#
# my_input_file.close()
#
# output_file = open('output.text', 'w')
#
# output_file.write("Hello, this is some output! ")
# output_file.close()

# BREAK 2:

# A - Write a function that takes as input a whole number and returns its fibonacci number

# B - Create an animal called cat, and write a function called meow(), that takes the cats name and prints: "{name}: meow..."

# C - Create two new files, a.txt and b.txt, and and put some stuff in a. Transfer everything to b.

# Practice HW:
#
# 1) If you didn't use python for your project 0, try to do the project again in Python.
#
# 2) Write a program that solves the FizzBuzz problem:
# 	For some n, write a function that produces the following:
# 	1
# 	2
# 	Fizz
# 	4
# 	Buzz
# 	Fizz
# 	7
# 	8
# 	Fizz
# 	Buzz
# 	11
# 	Fizz
# 	13
# 	14
# 	FizzBuzz
# 	...
# 	n
#
# 3) Create a Store class that sells a bunch of items, and you can purchase them with command line
# input. Display the price and allow the user to select which items they'd like. Some items include:
# 	Shoes
# 	Socks
# 	T-Shirt
# 	Pant
# 	Belt
#