class Animal:
	def __init__(self, name: str, weight: int):
		self.name = name
		self.weight = weight
	def get_weight_in_pounds(self):
		return self.name + " weighs " + str(self.weight) + "!"
	def get_type(name):
		if name == "Spot":
			return "Dog"
		else:
			return "Invertebrate"