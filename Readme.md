# This project is a series of examples of Python. Topics covered include:


+ 1 - Comments
+ 2 - Print Statements
+ 3 - Ints, Booleans and Strings
+ 4 - Type Conversions
+ 5 - Lists
+ 6 - Converting strings to lists
+ 7 - Dictionaries
+ 8 - Tuples
+ 9 - Sets
+ 10 - Slicing
+ BREAK 1 - Lists, Slicing, Type Conversions and Expressions
+ 11 - If/Elif/Else statements
+ 12 - While/For
+ 13 - Functions
+ 14 - Classes, constructors, instance and static methods
+ 15 - Importing and Exporting modules
+ 16 - Reading and Writing Files
+ BREAK 2 - Writing functions
